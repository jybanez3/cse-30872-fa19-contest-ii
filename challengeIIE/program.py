import sys

''' Function that given a target number and binary BFT in 
    BFS order (0 indicates no node), output all the paths that sum to the target. '''


RESULTS = []

def display(target):
    paths = sorted(RESULTS)
    for path in paths:
            print("{}: {}".format(target, ', '.join([str(i) for i in path])))

def traverse(BFT, node, path, target, sum_so_far):
    
    #increment path
    sum_so_far += BFT[node]
    end_node = False

    l = node * 2 + 1
    r = node * 2 + 2

    # Handle 0's
    if l < len(BFT):
        if BFT[l] == 0:
            if r < len(BFT):
                if BFT[r] == 0:
                    end_node = True
            else:
                end_node = True
    else:
        end_node = True


    if sum_so_far == target and end_node:
        RESULTS.append(path + [int(BFT[node])])
        return      


    # GO RIGHT
    if r < len(BFT): 
        if BFT[r] != 0:
            traverse(BFT, r, path + [int(BFT[node])], target, sum_so_far)
    # GO LEFT
    if l < len(BFT): 
        if BFT[l] != 0:
            traverse(BFT, l, path + [int(BFT[node])], target, sum_so_far)
    

    return


# Main Function
if __name__ == '__main__':

    while True:
        RESULTS = []
        target= sys.stdin.readline()
        # Breadth First Tree
        BFT = list(map(int, sys.stdin.readline().split()))
        
        if not target: 
            break
        
        target= int(target)
        
        traverse(BFT, 0, [], target, 0)
        display(target)
        
        
