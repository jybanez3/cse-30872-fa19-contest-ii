import sys

PATH = [[]]

def keep_track(matrix, x_size, y_size):
    global PATH
    paths = []
    col = x_size
    minimum = float("inf")

    for i in range(1, y_size+1):
        if minimum > PATH[i-1][x_size]:
            minimum = PATH[i-1][x_size]
            row = i - 1


    paths.append(row+1)
    print(PATH[row][col])

    while col != 1:
        
        if row is 0:
            minimum = min( PATH[row][col-1], PATH[row+1][col-1], PATH[y_size-1][col-1] )
            if minimum == PATH[row][col-1]:
                paths.append(row+1)
            elif minimum == PATH[row+1][col-1]:
                paths.append(row+2)
                row += 1
            elif minimum == PATH[y_size-1][col-1]:
                paths.append(y_size)
                row = y_size-1
        elif row is (y_size-1):
            minimum = min( PATH[row-1][col-1], PATH[row][col-1], PATH[0][col-1] )
            if minimum == PATH[0][col-1]:
                paths.append(1)
                row = 0
            elif minimum == PATH[row-1][col-1]:
                paths.append(row)
                row -= 1
            elif minimum == PATH[row][col-1]:
                paths.append(row+1)
        else:
            minimum = min( PATH[row-1][col-1], PATH[row][col-1], PATH[row+1][col-1] )
            if minimum == PATH[row-1][col-1]:
                paths.append(row)
                row -= 1
            elif minimum == PATH[row][col-1]:
                paths.append(row+1)
            elif minimum == PATH[row+1][col-1]:
                paths.append(row+2)
                row += 1

        col -= 1
    return paths

def keep_track_again(matrix, x_size, y_size):
    global PATH
    paths = []
    col = x_size

    minimum = float("inf")
    for i in range(1, y_size+1):
        if minimum > PATH[i-1][x_size]:
            minimum = PATH[i-1][x_size]
            row = i-1

    print(PATH[row][col])
    paths.append(row+1)
    while col != 1:
        minimum = min(PATH[0][col-1], PATH[1][col-1])

        if minimum == PATH[0][col-1]:
            paths.append(1)
            row = 0
            
        else:
            paths.append(2)
            row = 1
        col -= 1

    return paths

def truePath(matrix, x_size, y_size):
    global PATH
    PATH = [[float("inf") for _ in range(x_size+1)] for _ in range(y_size)]

    if x_size == 1:
        minimum = float("inf")
        i = 0
        for _ in range(1, y_size+1):
            if minimum > matrix[i][1]:
                minimum = matrix[i][1]
                minRow = i
            i += 1
        print(minimum)
        print(minRow+1)

    elif y_size > 2:
        i = 0
        for _ in matrix:
            PATH[i][1] = matrix[i][1]
            find1(i, 1, x_size, y_size)
            i += 1

        strace = [str(a) for a in keep_track(matrix, x_size, y_size)]
        
        print(' '.join(strace[::-1]))
            
    elif y_size == 1:
        val = 0
        PATH = []
        for l in matrix:
            for n in l:
                val += n
                PATH += ['1']
        PATH = PATH[1:]
        print(val)
        print(' '.join(PATH))
    
    elif y_size == 2:
        i = 0
        for _ in matrix:
            PATH[i][1] = matrix[i][1]
            find2(i, 1, x_size, y_size)
            i+= 1
        strace = [str(a) for a in keep_track_again(matrix, x_size, y_size)]
        print(' '.join(strace[::-1]))


def find2(row, col, x_size, y_size):
    global PATH

    if col >= x_size:
        return

    if (PATH[0][col+1] - matrix[0][col+1]) > PATH[row][col]:
        PATH[0][col+1] = matrix[0][col+1] + PATH[row][col]
        find1(0, col+1, x_size, y_size)

    if (PATH[1][col+1] - matrix[1][col+1]) > PATH[row][col]:
        PATH[1][col+1] = matrix[1][col+1] + PATH[row][col]
        find1(1, col+1, x_size, y_size)



def find1(row, col, x_size, y_size):
    global PATH

    if col == x_size:
        return

    if row is 0:
        
        if (PATH[y_size-1][col+1] - matrix[y_size-1][col+1]) > PATH[row][col]:
            PATH[y_size-1][col+1] = matrix[y_size-1][col+1] + PATH[row][col]
            find1(y_size-1, col+1, x_size, y_size)
        if (PATH[row][col+1] - matrix[row][col+1]) > PATH[row][col]:
            PATH[row][col+1] = matrix[row][col+1] + PATH[row][col]
            find1(row, col+1, x_size, y_size)
        if (PATH[row+1][col+1] - matrix[row+1][col+1]) > PATH[row][col]:
            PATH[row+1][col+1] = matrix[row+1][col+1] + PATH[row][col]
            find1(row+1, col+1, x_size, y_size)

    elif row is (y_size-1):

        if (PATH[row-1][col+1] - matrix[row-1][col+1]) > PATH[row][col]:
            PATH[row-1][col+1] = matrix[row-1][col+1] + PATH[row][col]
            find1(row-1, col+1, x_size, y_size)
        if (PATH[row][col+1] - matrix[row][col+1]) > PATH[row][col]:
            PATH[row][col+1] = matrix[row][col+1] + PATH[row][col]
            find1(row, col+1, x_size, y_size)
        if (PATH[0][col+1] - matrix[0][col+1]) > PATH[row][col]:
            PATH[0][col+1] = matrix[0][col+1] + PATH[row][col]
            find1(0, col+1, x_size, y_size)
    else:

        if (PATH[row-1][col+1] - matrix[row-1][col+1]) > PATH[row][col]:
            PATH[row-1][col+1] = matrix[row-1][col+1] + PATH[row][col]
            find1(row-1, col+1, x_size, y_size)
        if (PATH[row][col+1] - matrix[row][col+1]) > PATH[row][col]:
            PATH[row][col+1] = matrix[row][col+1] + PATH[row][col]
            find1(row, col+1, x_size, y_size)
        if (PATH[row+1][col+1] - matrix[row+1][col+1]) > PATH[row][col]:
            PATH[row+1][col+1] = matrix[row+1][col+1] + PATH[row][col]
            find1(row+1, col+1, x_size, y_size)

# Function to Create Matrix
def getMatrix(x_size, y_size):
    matrix = []
    for _ in range(y_size):
        matrix.append([0] + list(map(int, sys.stdin.readline().split())))

    return matrix


# Main Function
if __name__=='__main__':

    while True:
        try:
            y_size, x_size = map(int, sys.stdin.readline().strip().split())
        except ValueError:
            break
        
        matrix = getMatrix(x_size, y_size)
        truePath(matrix, x_size, y_size)
