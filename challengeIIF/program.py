import sys
 
''' A program that given a family tree, for each gift giver outputs who they need to get gifts for.  
    We are only concerned about the nieces and nephews of each person '''

def find_child(fam_dict,married, all_parents):
    recievers = {}

    while len(fam_dict) > 0:
        
        for p1 in fam_dict.keys():
            seen = False
            
            for p2 in fam_dict.keys():
                if p2 != p1:
                    # If a seen person is encountered
                    if p1 in fam_dict[p2]:
                        seen = True
                        break
                if seen:
                    break
            if seen:
                continue
            
            spouse = married[p1]
            for p2 in fam_dict.keys():
                # checking if spouse is a child
                if p2 != spouse:
                    if spouse in fam_dict[p2]:
                        seen = True
                        break
                if seen:
                    break

            
            if not seen:
                # Find nieces and nephews
                for c1 in fam_dict[p1]:
                    for c2 in fam_dict[p1]:
                        if c1 != c2:
                            if c1 in recievers and c2 in fam_dict:
                                recievers[c1].extend(fam_dict[c2])

                                if c1 in married:
                                    recievers[married[c1]].extend(fam_dict[c2])
                                    
                            elif c2 in fam_dict:
                                l = fam_dict[c2]
                                recievers[c1] = l[:]

                                if c1 in married:
                                    recievers[married[c1]] = l[:]

                del fam_dict[p1]
                del fam_dict[spouse]
                
                break

    for child in all_parents:
            if child in recievers:
                print("{} needs to buy gifts for: {}".format(child,', '.join(sorted(recievers[child]))))
            else:
                print("{} does not need to buy gifts".format(child))

def family_tree(group):

    married = {}
    fam_dict = {}

    for nuc_family in group:
        parents, children = nuc_family.split(":")

        parents = parents.split()
        parent = parents[0].strip()
        other_parent = parents[1].strip()

        married[ parent ] = other_parent
        married[ other_parent ] = parent

        children = children.split()
        children = [ i.strip() for i in children ]

        fam_dict[ parent ] = children[:]
        fam_dict[ other_parent ] = children[:]

    return married, fam_dict

if __name__ == '__main__':
    ext_family = list(sys.stdin)
    
    for _ in ext_family:
        if int(ext_family[0]) == 0:
            break

        # Family Groups
        nuc_family = int(ext_family[0])
        group = ext_family[1:1+nuc_family]
        ext_family = ext_family[1+nuc_family:]
        # Parent Groups
        parent = int(ext_family[0])
        all_parents = [ i.strip() for i in ext_family[1:1+parent] ]
        # Other Groups
        ext_family = ext_family[1+parent:]
        # make family tree out of dictionary
        married, fam_dict = family_tree(group)

        find_child(fam_dict,married, all_parents)
        
