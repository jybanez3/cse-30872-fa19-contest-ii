#!/usr/bin/env python3

'''
WORD MORPHS 

A word morph is a transformation from one word X to another word Y such that X and Y 
are words in the dictionary, and X can be transformed to Y by adding, deleting, or changing 
one letter. For instance, the transformations from dig to dog and from dog to dig are both word morphs.
'''

import sys 
import collections

def can_morph(str1, str2):
    l1 = len(str1)
    l2 = len(str2)
    if str1 > str2:
        return False
    if l1 == l2:
        diff = []
        for c in range(l1):
            if str1[c] != str2[c]:
                diff.append(c)
        
        if len(diff) == 1: 
            return True 
        else: 
            return False

    elif abs(l1 - l2) == 1:
        if len(set(str1).difference(set(str2))) == 0: 
            return True 
        elif len(set(str2).difference(set(str1))) == 0: 
            return True 
        else: 
            return False 

    return False

def find_longest(words):
    words = sorted(words)
    longest = []
    queue = []

    for word in words: 
        queue.append([word])
    while queue: 
        visited = queue.pop(0)
        if len(visited) < len(longest): 
            continue 
        else: 
            if len(visited) > len(longest):
                longest = visited
            for word in words: 
                if can_morph(visited[-1], word) and word not in visited: 
                    visited.append(word)
                    queue.append(list(visited))
                    visited.pop()
    return longest

if __name__ == '__main__': 
    words = []
    for line in sys.stdin: 
        words.append(line.strip())
    # print(words)
    x = find_longest(words)
    print(len(x))
    for thingy in x: 
        print(thingy.strip()) 