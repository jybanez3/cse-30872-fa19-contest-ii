#!/usr/bin/env python3

# Challenge IIA: Groups 

import sys
import collections

def build_graph(nodes): 
    ''' Build adjacency list representation of graph '''
    g = collections.defaultdict(list)
    for (x,y) in nodes:
        g[x].append(y)
        g[y].append(x)
    return g 

def DFS(g, temp, v, visited):
    if temp is None: 
        return
    if v in visited: 
        return 
    visited.append(v)
    temp.append(v)
    for u in g[v]:
        if u not in visited:
            temp = DFS(g, temp, u, visited)
    return temp

def find_groups(g, n_verts):
    visited = []
    groups = []
    temp = []
    for x in range(n_verts):
        if x not in visited: 
            temp = []
            groups.append(DFS(g, temp, x, visited))
    return groups

def format_output(groups, graph_count): 
    s = ''
    n = len(groups)
    if n == 1: 
        s = 'Graph {} has 1 group:'.format(graph_count)
    elif n > 1: 
        s = 'Graph {} has {} groups:'.format(graph_count, n)
    print(s)
    for x in sorted(groups): 
        print(' '.join(map(str, x)))

if __name__ == '__main__': 
    lines = [x.strip() for x in sys.stdin.readlines()]
    count = 0 
    graph_count = 0
    while count < len(lines): 
        graph_count += 1
        nodes = []
        n_verts = int(lines[count])
        n_edges = lines[count + 1]
        count += 2
        if n_edges:
            i = count
            for x in range(int(n_edges)): 
                nodes.append(map(int, lines[i].split()))
                i += 1               
        
        g = build_graph(nodes)
        x = [sorted(m) for m in find_groups(g, n_verts) if m != [0]]
        format_output(x, graph_count)

        # keep track of different test cases 
        count = count + int(n_edges)