#!/usr/bin/env python3 

'''
Given a pair of strings A and B, find the longest string X of letters such that 
there is a permutation of X that is a subsequence of A and there is a permutation 
of X that is a subsequence of B.
'''

import sys 

def find_perm(A, B): 
    listA = [x for x in A]
    listB = [y for y in B]

    # print(listA, listB)

    in_both = []

    for a in listA: 
        if a in listB:
            in_both.append(a)
            x = listB.index(a)
            listB.pop(x)

    return sorted(in_both)
    

if __name__ == '__main__': 
    count = 0
    lines = [x.strip() for x in sys.stdin.readlines()]
    while count < len(lines): 
        result = ''
        A = lines[count] 
        B = lines[count+1]
        x = find_perm(A, B)
        for c in x: 
            if c == '\n': 
                continue
            result += c
        print(result)

        count += 2

